# Pairing helper
This tool provides a dmenu selection to choose from predefined display modes, to easily switch screen settings.

## Prerequisites
- Dmenu
- xrandr
- arandr (optional)

## Setup
Just checkout the script and execute `./pairing.sh` you might want to bind it on a specific shortcut.

## Change/Add Mode
1. Open arandr.
2. Set up your screens as you like.
3. Press "Save As" and save it in the `modes` folder.

Your new mode should be available now.
