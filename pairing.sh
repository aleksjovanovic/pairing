#!/bin/bash

# Specific directories
REAL_PATH=$(realpath $0)
ABS_PATH=$(dirname $REAL_PATH)
MODES_DIR="${ABS_PATH}/modes"

# Selects mode from dmenu
function selectModeFromDmenu() {
	ls $MODES_DIR | sed -e "s/\.sh//g" | dmenu -l 5 -p "Modes: "
}

# Lists all external displays
function listSecondaryDisplays() {
  xrandr | grep -o '^[^ ]*' | grep -ve Screen -e eDP-1
}

function setupMainKeybaord() {
  setxkbmap -layout us -variant altgr-intl -option nodeadkeys -option ctrl:nocaps
}

function setupGuestKeyboard() {
  deviceId=$(xinput --list | grep HHKB | sed 's/.*\(id=[0-9]*\).*/\1/' | cut -d '=' -f 2)
  setxkbmap de
  setxkbmap -device $deviceId -layout us -variant altgr-intl -option nodeadkeys
}

# Get mode from Dmenu
SELECTED_MODE=$(selectModeFromDmenu)

echo $SELECTED_MODE

if [ "$SELECTED_MODE" != "" ]; then
	# execute mode
        ${MODES_DIR}/${SELECTED_MODE}.sh
fi

